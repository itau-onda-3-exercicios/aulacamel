package br.com.mastertech.aulacamel;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class IntegracaoArquivos {

	public static void main(String[] args) throws Exception {
		CamelContext contexto = new DefaultCamelContext();
		
		contexto.addRoutes(new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				
				errorHandler(defaultErrorHandler()
						.maximumRedeliveries(3)
						.redeliveryDelay(2000)
						.onExceptionOccurred(new Processor() {
							
							@Override
							public void process(Exchange exchange) throws Exception {
								String nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
								System.out.printf("\nDeu ruim na cópia do arquivo %s!", nomeArquivo);
								System.out.printf(" - Erro: " + exchange.getException().getMessage());
								
							}
						}));
				
				from("file://origem/?recursive=true")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						String nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
						System.out.printf("\nTentando copiar o arquivo %s",  nomeArquivo);
						
						exchange.getIn().setHeader(Exchange.FILE_NAME, nomeArquivo + ".bak");
					}
				})
				.to("file://destino/")
				.delay(10000)
				.end();
			}
		});
		
		contexto.start();
		
		for (;;) {}
	}
}
